import logo from "./logo.svg";
import "./App.css";
import {
  BrowserRouter as Router,
  Route,
  NavLink,
  BrowserRouter,
  Routes,
} from "react-router-dom";
import Login from "./containers/pages/Login";
import DataTable from "./containers/pages/DataTable";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/login" element={<Login />} />
          <Route path="/users" element={<DataTable />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
