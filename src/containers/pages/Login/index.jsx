import React, { useState } from "react";
import { Button, Card, Container, Form, Row } from "react-bootstrap";
import Swal from "sweetalert2";
import userJson from "../../../json/getData.json";
import { Router, redirect, useNavigate } from "react-router-dom";

export default function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const navigate = useNavigate();
  // useHistory

  const submitLogin = () => {
    const payload = {
      email: email,
      password: password,
    };
    const login = userJson.find(
      (user) => user.email == payload.email && user.password == payload.password
    );
    console.log(login);
    if (login) {
      Swal.fire("Success", "Berhasil Login", "success");
      navigate("/users");
      // redirect("/users");
    } else {
      Swal.fire("Failed", "Gagal Login", "warning");
      payload.email = "";
      payload.password = "";
    }
  };

  return (
    <Container>
      <Row className="justify-content-center py-5">
        <Card border="primary" style={{ width: "35rem" }}>
          <Card.Body>
            <Card.Title>
              <h3>Login Page</h3>
            </Card.Title>
            <Form>
              <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control
                  type="email"
                  onChange={(e) => setEmail(e.target.value)}
                  placeholder="Enter email"
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control
                  type="password"
                  onChange={(e) => setPassword(e.target.value)}
                  placeholder="Password"
                />
              </Form.Group>
              <Form.Group>
                <Button variant="primary" onClick={submitLogin} type="submit">
                  Login
                </Button>
              </Form.Group>
            </Form>
          </Card.Body>
        </Card>
      </Row>
    </Container>
  );
}
