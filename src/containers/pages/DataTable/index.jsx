import React, { useEffect, useState } from "react";
import { DataGrid } from "@mui/x-data-grid";
import axios from "axios";
import { Container, Row } from "react-bootstrap";

export default function DataTable() {
  const [user, setUser] = useState([]);
  const baseUrl = "https://jsonplaceholder.typicode.com/users";

  useEffect(() => {
    axios.get(`${baseUrl}`).then((response) => {
      setUser(
        response.data.map((i, item) => {
          return {
            id: i.id,
            company: i.company.name,
            address: i.address.city,
            name: i.username,
          };
        })
      );
    });
  });

  const columns = [
    { field: "id", headerName: "No", width: 100 },
    { field: "name", headerName: "User Name", width: 100 },
    { field: "company", headerName: "Company Name", width: 100 },
    { field: "address", headerName: "Address", width: 100 },
    {
      field: "Actions",
      options: {
        customBodyRender: (value, tableMeta, updateValue) => {
          return (
            <button onClick={() => console.log(value, tableMeta)}>Edit</button>
          );
        },
      },
    },
  ];

  return (
    <Container>
      <Row className="justify-content-center py-5">
        <div style={{ height: 400, width: "100%" }}>
          <DataGrid
            rows={user}
            columns={columns}
            pageSize={5}
            rowsPerPageOptions={[5]}
          />
        </div>
      </Row>
    </Container>
  );
}
